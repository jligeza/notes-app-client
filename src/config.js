const dev = {
  s3: {
    REGION: 'eu-central-1',
    BUCKET: 'notes-app-2-api-dev-attachmentsbucket-q7kzbx7mfxsl'
  },
  apiGateway: {
    REGION: 'eu-central-1',
    URL: 'https://j61yz6fmi6.execute-api.eu-central-1.amazonaws.com/dev'
  },
  cognito: {
    REGION: 'eu-central-1',
    USER_POOL_ID: 'eu-central-1_y8Xvnrmz4',
    APP_CLIENT_ID: '5sja57gfjro7al81gehvaijd14',
    IDENTITY_POOL_ID: 'eu-central-1:2af25ecc-fa22-41fc-aee5-f281287456e6'
  },
  STRIPE_KEY: 'pk_test_MLzl2HfhIAXQAZ8k7bb0Cx2700u4b30vbc'
}

const prod = {
  s3: {
    REGION: 'eu-central-1',
    BUCKET: 'notes-app-2-api-prod-attachmentsbucket-sexwgnc9oil8'
  },
  apiGateway: {
    REGION: 'eu-central-1',
    URL: 'https://og4rttxizi.execute-api.eu-central-1.amazonaws.com/prod'
  },
  cognito: {
    REGION: 'eu-central-1',
    USER_POOL_ID: 'eu-central-1_SiezVe6kU',
    APP_CLIENT_ID: '2vj6idh055jva9av3vdp2bjtre',
    IDENTITY_POOL_ID: 'eu-central-1:c150a76e-082e-4c3f-a116-b530c2fcad51'
  },
  STRIPE_KEY: 'pk_test_MLzl2HfhIAXQAZ8k7bb0Cx2700u4b30vbc'
}

// Default to dev if not set
const config = process.env.REACT_APP_STAGE === 'prod' ? prod : dev

export default {
  // Add common config values here
  MAX_ATTACHMENT_SIZE: 5000000,
  ...config
}
